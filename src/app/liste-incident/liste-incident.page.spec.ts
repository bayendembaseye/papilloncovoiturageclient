import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeIncidentPage } from './liste-incident.page';

describe('ListeIncidentPage', () => {
  let component: ListeIncidentPage;
  let fixture: ComponentFixture<ListeIncidentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeIncidentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeIncidentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
