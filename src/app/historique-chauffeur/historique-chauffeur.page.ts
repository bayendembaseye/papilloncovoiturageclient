import { Proposition } from './../model/proposition';
import { Storage } from '@ionic/storage';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { Direction } from '../model/direction';

@Component({
  selector: 'app-historique-chauffeur',
  templateUrl: './historique-chauffeur.page.html',
  styleUrls: ['./historique-chauffeur.page.scss'],
})
export class HistoriqueChauffeurPage implements OnInit {
  propositions: Proposition[];
  constructor(public storage: Storage, public api: ClientService) { }

  ngOnInit() {
    this.getHistorique();
  }
 getHistorique(){
  this.storage.get('id').then((val) => {
 this.api.getHistoriqueByChaffeur(val).subscribe((resp: Proposition[]) => {
   this.propositions = resp;
   for (let index = 0; index < this.propositions.length; index++) {
    const origine = new Direction();
    origine.lat = this.propositions[index].latdepart;
    origine.lng = this.propositions[index].longdepart;
    const destination = new Direction();
    destination.lat =  this.propositions[index].latarrive;
    destination.lng =  this.propositions[index].longarrive;
    this.propositions[index].origine = origine;
    this.propositions[index].destination = destination;

  }
 })
    
  });
 }
}
