import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistoriqueChauffeurPage } from './historique-chauffeur.page';

const routes: Routes = [
  {
    path: '',
    component: HistoriqueChauffeurPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistoriqueChauffeurPage]
})
export class HistoriqueChauffeurPageModule {}
