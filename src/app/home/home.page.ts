import { Paiement } from './../model/paiement';
import { Proposition } from './../model/proposition';
import { ClientModel } from './../model/client';
import { ClientService } from './../client.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Component, ViewChild, OnInit, ElementRef , OnDestroy, AfterViewInit } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification/ngx';

declare var window;
declare var google;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  address: string;
  height = 0;
  origin;
  destination;
  title = 'My first AGM project';
  lat = 51.678418;
  lng = 7.809007;
  lat1 = 14.725093;
  lat2  =  14.766395673518984;
  lng1 = -17.458555;
  lng2 = -17.21891530521566 ;
  locations: any;
  client_id;
  id;
  backButtonSubscription;


  constructor(public platform: Platform, public router: Router, public storage: Storage, public api: ClientService,
              public geolocation: Geolocation, public alertController: AlertController,
              public nativeGeocoder: NativeGeocoder, public backgroundMode: BackgroundMode,
              public localNotifications: LocalNotifications
              , public localNotification: PhonegapLocalNotification ) {

    console.log(platform.height());
    this.height = platform.height() - 56;
    this.locations = [];
    this.platform.ready().then(() => {
      this.localNotifications.on('click').subscribe(res => {
        const route = res.data ? res.data.secret : '';
        this.router.navigateByUrl(route);
      });
    });


  }

  ngOnInit(): void {
    this.verifier();


  }
  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
    }
    this.backButtonSubscription.unsubscribe();
  }

  // fonction qui verifie si l'utilisateur est déja inscrit
  verifier() {
    this.storage.get('id').then((val) => {
      console.log(val + 'jsd');
      if (val === null) {
      this.router.navigateByUrl('/menu/inscription');
     } else {
      this.getInfoClient(val);
      this.client_id = val;
      this.getDirection();
      this.updateCurrentPosition();
      this.id = setInterval(() => {
    this.updateCurrentPosition();
    this.getPropositionsToNotif();
   // this.getPaiementToNotif(val);
  }, 10000);

     }
    });
  }
  getDirection() {
    this.origin = {lat: 14.725093, lng: -17.458555};
    this.destination = {lat: 14.766395673518984, lng: -17.21891530521566};

  }

  getAddressFromCoords(lattitude, longitude) {
    console.log('getAddressFromCoords ' + lattitude + ' ' + longitude);
    const options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        this.address = '';
        const responseAddress = [];
        for (const [key, value] of Object.entries(result[0])) {
          if (value.length > 0) {
          responseAddress.push(value);
          }
        }
        responseAddress.reverse();
        for (const value of responseAddress) {
          this.address += value + ', ';
        }
        this.address = this.address.slice(0, -2);
      })
      .catch((error: any) => {
        console.log(error);
        this.address = 'Addresse non disponible!';
      });

  }
  // recupere la position actuelle de l'utilisateur

 async  updateCurrentPosition() {
    this.geolocation.getCurrentPosition().then(async (resp) => {
      if (this.client_id) {

        // if ( resp.coords.latitude!=null ){
          const positionForm = {
            client_id: this.client_id,
            latitude : resp.coords.latitude,
            longitude: resp.coords.longitude
          };
          this.api.createPosition(positionForm).subscribe((respo) => {
          },
          (error) => {
              console.log(error);
          });
       // }
      }
      // resp.coords.longituderesp.coords.latitude
      //
     }).catch((error) => {
       console.log('Error getting de position', error);
     });
   /*  const alert =  await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'tesxt',
      buttons: ['Cancel', 'Open Modal', 'Delete']
    });

     await alert.present();*/
}
getInfoClient(id) {

  this.api.getOneClientsAndType(id).subscribe((resp: ClientModel) => {
   if (resp == null) {
    this.router.navigateByUrl('/menu/inscription');
   }
  });

}
ngAfterViewInit() {
  this.backButtonSubscription = this.platform.backButton.subscribe(() => {
   ( navigator as any).app.exitApp();
  });
}
onMapReady($event) {
  const trafficLayer = new google.maps.TrafficLayer();
  trafficLayer.setMap($event);
  }

  // activer les nofications si il y'a n nouveau proposition

 getPropositionsToNotif() {
    this.api.getLastProposition().subscribe((res: Proposition) => {

      this.storage.get('idProposition').then((val) => {
        console.log(val + 'jsd');
        if ( res.id !== null && val !== res.id) {
          this.storage.set('idProposition', res.id);
          this.localNotifications.schedule({
            id: res.id,
            text: 'Nouveau trajet',
            // sound:'file://sound.mp3',
            data: { secret: '/menu/liste-proposition' }
          });
       }
      });
    });  
  }
  getPaiementToNotif(id) {
    this.api.getLastPaiement(id).subscribe((res: Paiement) => {

      this.storage.get('idPaiement').then((val) => {
        console.log(val + 'jsd');
        if ( res.id !== null && val !== res.id) {
          this.storage.set('idPaiement', res.id);
          this.localNotifications.schedule({
            id: res.id + res.montant,
            text: 'Votre Paiement de ' + res.montant + '€ à été reçu',
            // sound:'file://sound.mp3',
            //data: { secret: '/menu/liste-proposition' }
          });
       }
      });
    });  
  }
 /* simpleNotif() {
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' },
     // trigger: {in: 5 , unit: ELocalNotificationTriggerUnit.SECOND},

    });
  }*/

}
