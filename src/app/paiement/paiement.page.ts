import { Paiement } from './../model/paiement';
import { ClientModel } from './../model/client';
import { Stripe } from '@ionic-native/stripe/ngx';
import { Storage } from '@ionic/storage';
import { Reserver } from './../model/reserver';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.page.html',
  styleUrls: ['./paiement.page.scss'],
})
export class PaiementPage implements OnInit {
paiementForm: FormGroup;
reserverId;
reserver: Reserver;
numeroCarte = '' ;
mois;
annee;
cvc;
clientId;
stripe_key = 'pk_test_ty2BHn7VrEUSXxtxUiRReD2300YxPJpD56';
cardDetails;
client: ClientModel;
paiement = new Paiement();
error_messages = {
  numeroCarte: [{ type : 'required', message : 'le numéro de carte est obligatoire'},
                { type : 'pattern', message : 'le numéro de carte doit contenir seulement des nombres'}
      ],
  mois: [{ type : 'required', message : 'le mois  est obligatoire'},
      { type : 'pattern', message : 'le mois doit contenir seulement des nombres'},
      { type : 'min', message : 'le mois doit contenir au moins deux chiffres '},
      { type : 'max', message : 'le mois doit contenir  au plus quatre chiffres'}
],
  annee: [{ type : 'required', message : 'l\'année  est obligatoire'},
      { type : 'pattern', message : 'l\' année doit contenir seulement des nombres'},
      { type : 'min', message : 'le année doit contenir au moins quatre chiffres '},
      { type : 'max', message : 'l\' année doit contenir  au plus quatre chiffres'}


],
  cvc: [{ type : 'required', message : 'le CVC  est obligatoire'},
      { type : 'pattern', message : 'le CVC doit contenir seulement des nombres'},
      { type : 'min', message : 'le année doit contenir au moins trois chiffres '},
      { type : 'max', message : 'l\' année doit contenir  au plus trois chiffres'}
],
 };
 nombreReservation;
  constructor(private activitedRoute: ActivatedRoute, public formBuilder: FormBuilder,
              public api: ClientService, public storage: Storage, public stripe: Stripe,
              public router: Router,  public loadingController: LoadingController ) { }

  ngOnInit() {
    this.reserverId = this.activitedRoute.snapshot.paramMap.get('idReservation');
    this.paiementForm = this.formBuilder.group({
      montant: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      client_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      reserver_id: new FormControl('',  Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      numeroCarte: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      mois: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*'),
        Validators.min(1),
        Validators.max(12),

      ])),
      annee: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*'),
        Validators.min(2014),
        Validators.max(2100),
      ])),
      cvc: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*'),
        Validators.min(1),
        Validators.max(999),

      ])),
    });
    this.getClientId();
    this.getReservation();
  }
  getClientId() {
    this.storage.get('id').then((val) => {
      console.log(val + ' client')
      this.clientId = val;
      this.getInfosClient(val);
      this.getNombreDeReservation(val);
    });
  }
  getReservation() {
    this.api.getReservationsById(this.reserverId).subscribe((resp: Reserver) => {
       this.reserver = resp;
    });
  }
  async payer() {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    loading.present();
    this.stripe.setPublishableKey(this.stripe_key);

    this.cardDetails = {
        number: this.paiementForm.value.numeroCarte.replace(/\s/g, ''),
        expMonth: this.paiementForm.value.mois,
        expYear: this.paiementForm.value.annee,
        cvc: this.paiementForm.value.cvc
      };

    this.stripe.createCardToken(this.cardDetails)
        .then(token => {

          console.log(token);
          this.paiement.token_id = token.id;
          this.paiement.email = this.client.email;
          this.paiement.montant = this.reserver.proposition.montant * this.reserver.nbplaceres;
          // aplication de la reduction de 5%  si le client à deja 5 reservation
         /* if(this.nombreReservation > 5){
            this.paiement.montant = this.paiement.montant - ((this.paiement.montant * 5) / 100);
          }*/
          this.paiement.client_id = this.clientId;
          this.paiement.reserver_id = this.reserver.id;
          loading.dismiss();
          this.makePayment(this.paiement);
        })
        .catch(error => console.error(error));

  }
  async makePayment(paiement: Paiement) {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    loading.present();
    this.api.validerPaiement(paiement).subscribe((resp: any) => {
     if (resp.message === 'ok') {
      loading.dismiss();
        this.router.navigateByUrl('/une-reservations/' + this.paiement.reserver_id);
     } else {
      loading.dismiss();
     }
     console.log(resp);
   },
   (error) =>{
    loading.dismiss();
    console.log(error);
   });


  }
  getInfosClient(id) {
    this.api.getReservationsById(id).subscribe((resp: ClientModel) => {
      this.client = resp;
     // console.log('infos :' +  );
    });

  }
  getNombreDeReservation(id) {
    this.api.NombredeReservation(id).subscribe((resp) => {
      this.nombreReservation = resp;
      // console.log(resp);
    });
  }
  addSpace(event) {
    this.numeroCarte = this.numeroCarte.replace(/\s/g, '');
    this.numeroCarte = this.chunk(this.numeroCarte, 4).join(' ');
}

validate(event) {
    return event.charCode >= 48 && event.charCode <= 57;
}

chunk(str: any, position: number) {
  const ret = [];
  let len;

  for (let i = 0; i < str.length; i += position) {
     ret.push(str.substr(i, position));
  }

  return ret;
}
remplacer(mot: string) {
  for (let i = 0; i < 3; i ++) {
    console.log(mot.replace(' ', ''));
  }
  return mot;
}

}
