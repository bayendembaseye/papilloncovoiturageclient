import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { TypeClient } from '../model/typeClient';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  inscriptionForm: FormGroup;
  client: any;
  typeClients: TypeClient[];
  error_messages = {
    nom: [{ type : 'required', message : 'le nom est obligatoire'},
          {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
        ],
    prenom: [{ type : 'required', message : 'le prenom est obligatoire'},
          {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
        ],
    adresse: [{ type : 'required', message : 'l\'adresse est obligatoire'},
        {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
      ],
    telephone: [{ type : 'required', message : 'le telephone est obligatoire'},
      {type: 'pattern', message: 'le champs doit contenir seulement des chiffres'}
    ],
    email: [//{ type : 'required', message : 'l\'email est obligatoire'},
      {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
    ],
    sexe: [{ type : 'required', message : 'le sexe est obligatoire'},
      {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
    ],
    siren: [{ type : 'minLength', message : 'le siren  doit etre supérieur à 5 caractères'},
      {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
    ],
    siret: [{ type : 'minLength', message : 'le siret doit etre supérieur à 5 caractères'},
      {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
    ],
    numerocarte: [{ type : 'minLength', message : 'le numerocarte doit etre supérieur à 5 chiffres'},
      {type: 'pattern', message: 'le champs doit contenir seulement des chiffres'}
    ],
    typeclient_id: [{ type : 'required', message : 'le typeclient_id est obligatoire'},
      {type: 'pattern', message: 'le champs doit contenir seulement  des chiffres'}
    ]

  };

  constructor(public api: ClientService, private storage: Storage, private route: Router,
              public formBuilder: FormBuilder, public loadingController: LoadingController
  ) {
    this.inscriptionForm = this.formBuilder.group({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      prenom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      adresse: new FormControl('', Validators.compose([
        Validators.required
      ])),
      telephone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
       Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      sexe: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9]*')
      ])),
      siren: new FormControl('', Validators.compose([
        Validators.minLength[5],
        Validators.pattern('^[a-zA-Z0-9]*')
      ])),
      siret: new FormControl('', Validators.compose([
        Validators.minLength[5],
        Validators.pattern('^[a-zA-Z0-9]*')
      ])),
      numerocarte: new FormControl('', Validators.compose([
        Validators.minLength[5],
        Validators.pattern('^[0-9]*')
      ])),
      typeclient_id: new FormControl()
      ,
    });
  }

  ngOnInit() {
    this.getTypeClient();
  }

  async register() {
    this.inscriptionForm.value['typeclient_id'] = 1;
    console.log(this.inscriptionForm.value);
    const loading = await this.loadingController.create({
      message: 'Patientez...'

    });
    await loading.present();
    await this.api.createClient(this.inscriptionForm.value)
      .subscribe(res => {
          /*let id = res['id'];
          this.router.navigate(['/detail/'+id]);*/
          loading.dismiss();
          this.storage.set('id', res.id);
          this.route.navigateByUrl('/menu/home');
        }, (err) => {
          console.log(err);
          loading.dismiss();
        });
  }
  async getClient() {
    const loading = await this.loadingController.create({
      message: 'Loading'

    });
    await loading.present();
    await this.api.getClient()
      .subscribe(res => {
        console.log(res);
        this.client = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }
  
  async getTypeClient() {
    await this.api.getTypeClient()
      .subscribe(res => {
        console.log(res);
        this.typeClients= res;
      }, err => {
        console.log(err);
      });
  }

}
