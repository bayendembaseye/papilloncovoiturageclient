export class Demande {
    public datedepart: string;
    public adressedepart: string;
    public adressearrive: string;
    public latdepart: number;
    public lngdepart: number;
    public latarrive: number;
    public lngarrive: number;
    public etat: boolean;
    public client_id: number;
    public nbplace: number;
}