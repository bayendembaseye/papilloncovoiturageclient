import { Reserver } from './reserver';
import { ClientModel } from './client';
export class Paiement{
    public id: number;
    public montant: number;
    public client_id: number;
    public reserver_id: number;
    public client: ClientModel;
    public reserver: Reserver;
    public token_id: string;
    public email: string;
}