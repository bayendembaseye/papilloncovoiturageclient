import { Direction } from './direction';
import { ClientModel } from './client';
import { Proposition } from './proposition';
export class Reserver {
    public id: number;
    public date: Date;
    public proposition_id: number;
    public client_id: number;
    public nbplaceres: number;
    public proposition: Proposition;
    public client: ClientModel;
    public origine?: Direction;
    public destination?: Direction;
    public etat: boolean;
    public paye: boolean;
}
