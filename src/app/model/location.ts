import { MarkerModel } from './marker';
export class LocationModel {
    public lat: number;
    public lng: number;
    public viewport?: Object;
    public zoom: number;
    public address_level_1?:string;
    public address_level_2?: string;
    public address_country?: string;
    public address_zip?: string;
    public address_state?: string;
    public marker?: MarkerModel;
}