import { Positions } from './position';
import { TypeClient } from './typeClient';
export class ClientModel {
    public id: number;
    public nom: string;
    public prenom: string;
    public adresse: string;
    public telephone: number;
    public sexe: string;
    public email: string;
    public siren: number;
    public siret: number;
    public numerocarte: number;
    public typeclient_id: number;
    public typeclient: TypeClient;
    public positions: Positions[];
}