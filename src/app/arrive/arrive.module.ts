import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArrivePage } from './arrive.page';

const routes: Routes = [
  {
    path: '',
    component: ArrivePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      //apiKey: 'AIzaSyBb6fFkz58oVRSegXQzxUTlqaBFNN95AA0' //for browser
      //apiKey: 'AIzaSyALSLqyG1ATFGNEOMgGfuLMm2vXTITyqXg' // pour android
     // apiKey: 'AIzaSyAd9YIATdY0H83OpK9Xm_ne6hhA_jMrxHA'
     apiKey: 'AIzaSyDJby-hPhgoq4hIhiwKiHYvYmEUn74qnBw',
     // apiKey: 'AIzaSyAnku-8yhTOiYJ9sn1QF0wAU3Qtvlt6GIM'
     libraries: ['places']
   })

  ],
  declarations: [ArrivePage]
})
export class ArrivePageModule {}
