import { ArrivePage } from './../arrive/arrive.page';
import { Demande } from './../model/demande';

import { Storage } from '@ionic/storage';
import { DepartPage } from './../depart/depart.page';
import { TypeVehicule } from './../model/typeVehicule';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { TypeProposition } from '../model/typeproposition';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-proposition',
  templateUrl: './proposition.page.html',
  styleUrls: ['./proposition.page.scss'],
})
export class PropositionPage implements OnInit {
propositionForm: FormGroup;
dataReturned;

nbplace = 0;
demande = new Demande();
error_messages = {
  nbplace: [{ type : 'required', message : 'le champ est obligatoire'},
        {type: 'pattern', message: 'le champs doit contenir seulement des  chiffres'}
      ],
  adressedepart: [{ type : 'required', message : 'le champs est obligatoire'},

  ],
  adressearrive: [{ type : 'required', message : 'le champs est obligatoire'}
  ],
  datedepart: [{ type : 'required', message : 'le champs est obligatoire'}
  ],

};
  constructor(public formBuilder: FormBuilder, public loadingController: LoadingController,
              private route: Router, private api: ClientService, private modalCtrl: ModalController,
              private clientService: ClientService, private storage: Storage, private datePipe: DatePipe,) {

              }

  ngOnInit() {
    this.propositionForm = this.formBuilder.group({
      nbplace: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      client_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      adressedepart: new FormControl('', Validators.compose([
        Validators.required
      ])),
      adressearrive: new FormControl('', Validators.compose([
        Validators.required
      ])),
      datedepart: new FormControl('', Validators.compose([
        Validators.required
      ])),
      latdepart: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lngdepart: new FormControl('', Validators.compose([
         Validators.required
      ])),
      latarrive: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lngarrive: new FormControl('', Validators.compose([
       Validators.required
      ])),
    });
    /*this.latdepart = 545646;
    this.longdepart = 545646;
    this.latarrive = 545646;
    this.longarrive = 545646;*/
    this.storage.get('id').then((val) => {
      console.log(val + 'jsd');
      this.demande.client_id = val;
    });
  }


   async register() {
    // this.propositionForm.value.datedepart = this.datePipe.transform(this.propositionForm.value.datedepart, "yyyy-MM-dd HH:mm")
    this.demande.datedepart = this.datePipe.transform(this.propositionForm.value.datedepart, "yyyy-MM-dd HH:mm");
    console.log( this.demande.datedepart + 'test');
    this.demande.etat = false;
    const loading = await this.loadingController.create({
      message: 'Patientez...'

    });
    await loading.present();

    this.api.createDemande(this.demande).subscribe((resp) => {

     //console.log(this.de);
     console.log(resp);
     loading.dismiss();
    }, (err) => {
      console.log(err.data);
      loading.dismiss();
    });
    }
  async openStart() {
    const modal = await this.modalCtrl.create({
      component: DepartPage,
      componentProps: {
        paramID: 123,
        paramTitle: 'Test Title'
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        alert('Modal Sent Data :' + dataReturned.data.latdepart);
        this.demande.latdepart = dataReturned.data.latdepart;
        this.demande.lngdepart = dataReturned.data.lngdepart;
        this.demande.latarrive = dataReturned.data.latarrive;
        this.demande.lngarrive = dataReturned.data.lngarrive;
        this.demande.adressedepart = dataReturned.data.adressedepart;
        this.demande.adressearrive = dataReturned.data.adressearrive;
      }
    });

    return await modal.present();
  }
  /*async openEnd() {
    const modal = await this.modalCtrl.create({
      component: ArrivePage,
      componentProps: {
        paramID: 123,
        paramTitle: 'Test Title'
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        alert('Modal Sent Data :' + dataReturned.data.latdepart);
     /*   this.demande.latdepart = dataReturned.data.latdepart;
        this.demande.lngdepart = dataReturned.data.lngdepart;
        this.demande.latarrive = dataReturned.data.latarrive;
        this.demande.lngarrive = dataReturned.data.lngarrive;
        this.demande.adressedepart = dataReturned.data.adressedepart;
        this.demande.adressearrive = dataReturned.data.adressearrive;
      }
    });

    return await modal.present();
  }
*/


}
