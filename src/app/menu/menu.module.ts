import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [{
      path: 'home',
      loadChildren: '../home/home.module#HomePageModule'
    },
    {
      path: 'inscription',
      loadChildren: '../inscription/inscription.module#InscriptionPageModule'
    },
    {
      path: 'profil',
      loadChildren: '../profil/profil.module#ProfilPageModule'
    },
    {
      path: 'proposition',
      loadChildren: '../proposition/proposition.module#PropositionPageModule'
    },
    { path: 'liste-proposition',
    loadChildren: '../liste-proposition/liste-proposition.module#ListePropositionPageModule' },
    { path: 'mes-propositions', loadChildren: '../mes-propositions/mes-propositions.module#MesPropositionsPageModule' },
    { path: 'mes-reservations', loadChildren: '../mes-reservations/mes-reservations.module#MesReservationsPageModule' },
    { path: 'mes-transport', loadChildren: '../mes-transport/mes-transport.module#MesTransportPageModule' },
    { path: 'historique-chauffeur', loadChildren: '../historique-chauffeur/historique-chauffeur.module#HistoriqueChauffeurPageModule' },
    { path: 'signaler', loadChildren: '../signaler/signaler.module#SignalerPageModule' }
  ]
  },
  {
    path: '',
    redirectTo: 'menu/home'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
