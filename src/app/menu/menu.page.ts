import { ClientModel } from './../model/client';
import { ClientService } from './../client.service';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  id;
  pages = [{
    title: 'Acceuil',
    url: '/menu/home',
    icon: 'home'
  },
  {
    title: 'Profil',
    url: '/menu/profil',
    icon: 'person'
  },
  /*{
    title: 'Inscription',
    url: '/menu/inscription'
  },*/
  
  {
    title: 'Liste des trajets',
    url: '/menu/liste-proposition',
    icon: 'eye'
  },
  {
    title: 'Mes Reservations',
    url: '/menu/mes-reservations',
    icon: 'eye'
  },
  {
    title: 'demande',
    url: '/menu/proposition',
    icon: 'add-circle'
  },
  {
    title: 'Mes demandes',
    url: '/menu/mes-propositions',
    icon: 'eye'
  },
  {
    title: 'Signaler',
    url: '/menu/signaler',
    icon: 'warning'
  }
];
pageChauffeur = [
    {
      title: 'Profil',
      url: '/menu/profil'
    },
    {
      title: 'Mes Transport',
      url: '/menu/mes-transport'
    },
    {
      title: 'Historique',
      url: '/menu/historique-chauffeur'
    },
  {
    title: 'Signaler',
    url: '/menu/signaler'
  }

];
selectedPath = '';
client = new ClientModel();
  constructor(private router: Router, private storage: Storage,
              private api: ClientService) {
                this.verifierProfil();
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath =  event.url;
    });
  }


  ngOnInit() {
    
  }
  verifierProfil(){
    this.storage.get('id').then((val) => {
      this.api.getOneClientsAndType(val).subscribe((resp: ClientModel) => {
          this.client = resp;
      });
    });
  
  }
 

}
