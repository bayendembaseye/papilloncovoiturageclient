import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: '/menu/home', pathMatch: 'full' },
    // { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  //{ path: 'inscription', loadChildren: './inscription/inscription.module#InscriptionPageModule' },
  { path: '', loadChildren: './menu/menu.module#MenuPageModule' },
  //{ path: 'liste-proposition', loadChildren: './liste-proposition/liste-proposition.module#ListePropositionPageModule' },
  //{ path: 'depart', loadChildren: './depart/depart.module#DepartPageModule' },
  //{ path: 'arrive', loadChildren: './arrive/arrive.module#ArrivePageModule' },
  //{ path: 'proposition', loadChildren: './proposition/proposition.module#PropositionPageModule' },
  //{ path: 'profil', loadChildren: './profil/profil.module#ProfilPageModule' },
  { path: 'une-proposition/:id', loadChildren: './une-proposition/une-proposition.module#UnePropositionPageModule' },
  { path: 'reserver', loadChildren: './reserver/reserver.module#ReserverPageModule' },
  { path: 'une-reservations/:id', loadChildren: './une-reservations/une-reservations.module#UneReservationsPageModule' },
  { path: 'liste-incident', loadChildren: './liste-incident/liste-incident.module#ListeIncidentPageModule' },
  { path: 'paiement/:idReservation', loadChildren: './paiement/paiement.module#PaiementPageModule' },
  //{ path: 'signaler', loadChildren: './signaler/signaler.module#SignalerPageModule' },
  //{ path: 'historique-chauffeur', loadChildren: './historique-chauffeur/historique-chauffeur.module#HistoriqueChauffeurPageModule' },
 // { path: 'mes-transport', loadChildren: './mes-transport/mes-transport.module#MesTransportPageModule' },
  //{ path: 'mes-reservations', loadChildren: './mes-reservations/mes-reservations.module#MesReservationsPageModule' },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
