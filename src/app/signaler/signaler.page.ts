import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { Signaler } from './../model/signaler';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-signaler',
  templateUrl: './signaler.page.html',
  styleUrls: ['./signaler.page.scss'],
})
export class SignalerPage implements OnInit {
signalerForm: FormGroup;
signale = new Signaler();
error_messages = {
  contenu: [{ type : 'required', message : 'le contenu est obligatoire'}
      ],
      adresse: [{ type : 'required', message : 'l\'adresse est obligatoire'}
    ], }
  constructor(private formBuilder: FormBuilder, private api: ClientService,
    private loader: LoadingController, public storage:Storage) { }

  ngOnInit() {
    this.signalerForm = this.formBuilder.group({
      client_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*'),
      ])),
      contenu: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required)
    });
    this.storage.get('id').then( (val) => {
      this.signale.client_id = val;
    });
  }
   signaler(){
    //let loading = this.loader.create()
    this.api.createSignaler(this.signale).subscribe((resp) => {

    },
    (error) => {
      console.log(error);
    });
  }

}
