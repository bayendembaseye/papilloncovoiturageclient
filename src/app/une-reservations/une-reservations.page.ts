import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { Note } from './../model/note';
import { Storage } from '@ionic/storage';
import { Positions } from './../model/position';
import { Platform, AlertController, LoadingController } from '@ionic/angular';
import { ClientService } from './../client.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Reserver } from '../model/reserver';
import { ClientModel } from '../model/client';


declare var google;
//var distance = require('google-distance');
@Component({
  selector: 'app-une-reservations',
  templateUrl: './une-reservations.page.html',
  styleUrls: ['./une-reservations.page.scss'],
})
export class UneReservationsPage implements OnInit {
  id;
reserverId;
depart;
arrive;
reserver: Reserver;
@ViewChild('map') mapElement: ElementRef;
map: any;
address: string;
height = 0;
position: Positions;
client = new ClientModel();
chaufeurPosition;
clientPosition;;
client_id;
iconeChauffeur= "assets/icon/chauffeur.png";
public markerOptions = {
  origin: {
      infoWindow: 'Départ.',
      icon: 'assets/icon/departs.png',
      //draggable: true,
  },
  destination: {
      icon: 'assets/icon/arrive.png',
      label: '',
      opacity: 0.8,
  },
}
public markerOptions1 = {
  origin: {
      infoWindow: 'Départ.',
      icon: 'assets/icon/chauffeur.png',
      //draggable: true,
  },
  destination: {
      icon: 'assets/icon/departs.png',
      label: '',
      opacity: 0.8,
  },
}
public renderOptions = {
  suppressMarkers: true,
}

public provideRouteAlternatives = true; // default: false
// directionsService = new google.maps.DirectionsService;
// directionsDisplay = new google.maps.DirectionsRenderer;
@ViewChild('myPanel') directionsPanel: ElementRef;
  constructor(private activitedRoute: ActivatedRoute, public api: ClientService,
              public platform: Platform, private storage: Storage, private alertController: AlertController,
              public loadingController: LoadingController, public sms: SMS, public callNumber: CallNumber) { }

  ngOnInit() {
    this.reserverId = this.activitedRoute.snapshot.paramMap.get('id');
    this.getReservation();
    console.log(this.platform.height());
    this.height = this.platform.height() - 56;
    this.storage.get('id').then( (val) => {
      this.client_id = val;
      this.getInfoClient(val);
    });
  }
   async getReservation() {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    await loading.present();
    await this.api.getReservationsById(this.reserverId).subscribe((resp: Reserver) => {
       this.reserver = resp;
       //console.log(resp + 'te');
       loading.dismiss();
       this.depart = { lat: resp.proposition.latdepart, lng: resp.proposition.longdepart};
       this.arrive = { lat: resp.proposition.latarrive, lng: resp.proposition.longarrive};
       this.getPositionChauffeur(this.reserver.proposition.client_id);
       this.id = setInterval(() => {
        this.getPositionChauffeur(this.reserver.proposition.client_id);
      }, 10000);
      // this.loadMap();

       // this.startNavigating();
    },
    (error) => {
      loading.dismiss();
    });
  }
  getPositionChauffeur(id) {
    this.api.getPositionByChauffeur(id).subscribe((respo: Positions) => {
    this.position = respo;
    console.log(this.position);
    //this.getDistance(this.position.latitude +''+ this.position.longitude,this.reserver.client.)
    this.chaufeurPosition = {lat: this.position.latitude, lng: this.position.longitude};
    this.clientPosition = {lat: this.reserver.proposition.latdepart, lng: this.reserver.proposition.longdepart};
    });

  }

  loadMap() {

      const latLng = new google.maps.LatLng(this.reserver.proposition.latdepart, this.reserver.proposition.longdepart);
      const mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };


      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    /*  this.map.addListener('tilesloaded', () => {
        console.log('accuracy',this.map);
      });*/

  }
 /* startNavigating(){

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    directionsDisplay.setMap(this.map);
    directionsDisplay.setPanel(this.directionsPanel.nativeElement);
    let latLngdepart = new google.maps.LatLng(this.reserver.proposition.latdepart, this.reserver.proposition.longdepart);
    let latLngarrive = new google.maps.LatLng(this.reserver.proposition.latarrive, this.reserver.proposition.longarrive);
    directionsService.route({
        origin: latLngdepart,
        destination:latLngarrive ,
        travelMode: google.maps.TravelMode['DRIVING']
    }, (res, status) => {

        if(status == google.maps.DirectionsStatus.OK){
            directionsDisplay.setDirections(res);
        } else {
            console.warn(status);
        }

    });

}
*/
getInfoClient(id) {
  this.api.getOneClientsAndType(id).subscribe((resp: ClientModel) => {
    this.client = resp;
    console.log(resp);
  });
}
async createNote(chaffeur) {
  const alert = await this.alertController.create({
    header: 'Checkbox',
    inputs: [
      {
        name: 'radio1',
        type: 'radio',
        label: '1/5',
        value: 1,
        checked: true
      },
      {
        name: 'radio2',
        type: 'radio',
        label: '2/5',
        value: 2
      },
      {
        name: 'radio3',
        type: 'radio',
        label: '3/5',
        value: 3
      },
      {
        name: 'radio4',
        type: 'radio',
        label: '4/5',
        value: 4
      },
      {
        name: 'radio5',
        type: 'radio',
        label: '5/5',
        value: 5
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Ok',
        handler: (data) => {
          this.saveNote(data, chaffeur);
          console.log('Confirm Ok' + data);
        }
      }
    ]
  });

  await alert.present();
}
async saveNote(note,  chauffeur_id) {
  const loading = await this.loadingController.create({
    message: 'Veuillez Patientez !!!'
  });
  await loading.present();
  const noter = new Note();
  noter.client_id = chauffeur_id;
  noter.evaluateur = this.client_id;
  noter.note = note;
  this.api.createNote(noter).subscribe((resp: Note) => {
    loading.dismiss();
  },
  (error) => {
    loading.dismiss();

  });

}
callChaffeur() {
  this.callNumber.callNumber(this.reserver.proposition.client.telephone + '', true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
}
async sendMessage() {
  const alert = await this.alertController.create({
    header: 'Saisir votre message',
    inputs: [
     {
      name: 'name1',
      type: 'text',
      placeholder: 'Saisisez votre message'
     }
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Ok',
        handler: (data) => {
          this.sms.send(this.reserver.proposition.client.telephone + '', data);
        }
      }
    ]
  });

  await alert.present();
}
/*getDistance(origine, destinations){
  distance.get(
    {
      index: 1,
      origin: origine,
      destination: destinations
    },
    function(err, data) {
      if (err) return console.log(err);
      console.log(data);
    });
}*/
}
