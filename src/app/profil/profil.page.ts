import { Storage } from '@ionic/storage';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { ClientModel } from '../model/client';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {
  client = new ClientModel();
  id;
  nombreReservation;
  constructor( public api: ClientService, public storage: Storage) { }

  ngOnInit() {
    this.getClient();
  }
  getClient() {
    this.storage.get('id').then((val) => {
      if (val !== '') {
        this.api.getOneClients(val).subscribe((resp: ClientModel) => {
         this.client = resp;
         this.getNombreDeReservation(val);
         console.log(resp);
        },
        (error) =>{
          console.log(error);
        });
      }
     });
   
  }
  getNombreDeReservation(id){
    this.api.NombredeReservation(id).subscribe((resp) => {
      this.nombreReservation = resp;
      //console.log(resp);
    });
  }
}
