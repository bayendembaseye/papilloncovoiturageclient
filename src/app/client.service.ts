import { Paiement } from './model/paiement';
import { Signaler } from './model/signaler';
import { Note } from './model/note';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
//http://127.0.0.1:8000/api
//http://127.0.0.1:81/guyaneBackend/public/api
const apiUrl = "http://127.0.0.1/guyaneBackend/public/api";
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error.message}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  getClient(): Observable<any> {
    const url = `${apiUrl}/client`;
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }
  getOneClients(id) {
    return this.http.get(apiUrl + '/client/' + id) ;
  }
  getOneClientsAndType(id) {
    return this.http.get(apiUrl + '/client-and-type/' + id) ;
  }
  createClient(data): Observable<any> {
    const url = `${apiUrl}/client/create`;
    return this.http.post(url, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getTypeClient(): Observable<any> {
    const url = `${apiUrl}/type-client`;
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getAllVehicule(){
    return this.http.get(apiUrl + '/type-vehicule');
  }

  getAllTypeproposition(){
    return this.http.get(apiUrl + '/type-proposition')
  }
  createDemande(data): Observable<any> {
    const url = `${apiUrl}/demande/create`;
    return this.http.post(url, data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getProposition(){
    return this.http.get(apiUrl + '/proposition/liste');
    }
    uneProposition(id){
      return this.http.get(apiUrl + '/proposition/une/' + id);
      }

      createReserver(data){
        const url = `${apiUrl}/reserver/create`;
        return this.http.post(url, data, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
      }
      createPosition(data){
        const url = `${apiUrl}/position/create`;
        return this.http.post(url, data, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
      }
      getReservationsByClient(id) {
        return this.http.get(apiUrl + '/mes-reservations/' + id) ;
      }
      getReservationsById(id) {
        return this.http.get(apiUrl + '/une-reservations/' + id) ;
      }
      getPositionByChauffeur(id) {
        return this.http.get(apiUrl + '/position/' + id) ;
      }
      getPropositionByChauffeur(id){
        return this.http.get(apiUrl + '/proposition/chauffeur/' + id) ;
      }
      confirmProposition(id){
        return this.http.get(apiUrl + '/proposition/valider/' + id);
      }
      getClientByproposition(id){
        return this.http.get(apiUrl + '/reservation/clients/' + id);
      }
      getHistoriqueByChaffeur(id){
        return this.http.get(apiUrl + '/proposition/historique/' + id);
      }
      createNote(note: Note){
        const url = `${apiUrl}/note/create`;
        return this.http.post(url, note, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
      }
      createSignaler(signaler: Signaler){
        const url = `${apiUrl}/signaler/create`;
        return this.http.post(url, signaler, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
      }
      getIncident(): Observable<any> {
        const url = `${apiUrl}/signaler`;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
      validerPaiement(paiement: Paiement){
        const url = `${apiUrl}/paiement/valider`;
        return this.http.post(url, paiement, httpOptions)
          .pipe(
            catchError(this.handleError)
          );
      }
      NombredeReservation(id): Observable<any> {
        const url = `${apiUrl}/reservation/counts/` + id;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
      getPropositionToNotif(): Observable<any> {
        const url = `${apiUrl}/proposition/notif`;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
      updatePropositionNotif($id): Observable<any> {
        const url = `${apiUrl}/proposition/modifier/notification/` + $id;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
      getLastProposition(): Observable<any> {
        const url = `${apiUrl}/proposition/last` ;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
      getLastPaiement(id): Observable<any> {
        const url = `${apiUrl}/paiement/last/` + id ;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
      getDistanceBetween(source, destination): Observable<any> {
        const url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${source}&destinations=${destination}&key=AIzaSyDJby-hPhgoq4hIhiwKiHYvYmEUn74qnBw` ;
        return this.http.get(url, httpOptions).pipe(
          map(this.extractData),
          catchError(this.handleError));
      }
}
