import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesPropositionsPage } from './mes-propositions.page';

describe('MesPropositionsPage', () => {
  let component: MesPropositionsPage;
  let fixture: ComponentFixture<MesPropositionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesPropositionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesPropositionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
