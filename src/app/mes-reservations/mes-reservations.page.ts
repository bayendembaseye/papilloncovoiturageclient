import { LoadingController } from '@ionic/angular';
import { Direction } from './../model/direction';
import { Reserver } from './../model/reserver';
import { ClientService } from './../client.service';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mes-reservations',
  templateUrl: './mes-reservations.page.html',
  styleUrls: ['./mes-reservations.page.scss'],
})
export class MesReservationsPage implements OnInit {
id;
reservations: Reserver[];
  constructor(public storage: Storage, public api: ClientService, public loadingController: LoadingController) { }

  ngOnInit() {
    this.mesReservations();
  }

  
  async mesReservations() {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    await loading.present();
    this.storage.get('id').then((val) => {
      this.api.getReservationsByClient(val).subscribe((resp: Reserver[]) => {
        this.reservations = resp;
        for (let index = 0; index < this.reservations.length; index++) {
          let origine = new Direction();
          origine.lat = this.reservations[index].proposition.latdepart;
          origine.lng = this.reservations[index].proposition.longdepart;
          let destination = new Direction();
          destination.lat =  this.reservations[index].proposition.latarrive;
          destination.lng =  this.reservations[index].proposition.longarrive;
          this.reservations[index].origine = origine;
          this.reservations[index].destination = destination;
        }
        loading.dismiss();
       // console.log(resp);
      },
      (error) => {
        loading.dismiss();

      });
      //this.id = val;
    });
  }

}
