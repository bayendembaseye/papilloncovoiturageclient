import { Storage } from '@ionic/storage';
import { Proposition } from './../model/proposition';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-mes-transport',
  templateUrl: './mes-transport.page.html',
  styleUrls: ['./mes-transport.page.scss'],
})
export class MesTransportPage implements OnInit {
propositions: Proposition[];
  constructor(private api: ClientService, private storage: Storage, public alertController: AlertController) { }

  ngOnInit() {
    this.getPropositionByChauffeur();
  }
  
  getPropositionByChauffeur() {
    this.storage.get('id').then((val) => {
      this.api.getPropositionByChauffeur(val).subscribe((resp: Proposition[]) => {
        this.propositions = resp;
      });
    });
  }
  async confirmerTransport(id){
    const alert = await this.alertController.create({
      header: 'Confirmer!',
      message: 'Vous avez déja effectuer le trajet ?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ok',
          handler: () => {
            this.api.confirmProposition(id).subscribe((resp) => {
              console.log(resp);
            });
          }
        }
      ]
    });

    await alert.present();
   
  }
}
