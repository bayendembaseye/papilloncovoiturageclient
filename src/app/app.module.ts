import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification/ngx';
import { SMS } from '@ionic-native/sms/ngx';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import {DatePipe} from '@angular/common';
import {Stripe} from '@ionic-native/stripe/ngx';
import { from } from 'rxjs';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule,
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      //apiKey: 'AIzaSyBb6fFkz58oVRSegXQzxUTlqaBFNN95AA0' //for browser
      //apiKey: 'AIzaSyALSLqyG1ATFGNEOMgGfuLMm2vXTITyqXg' // pour android
     // apiKey: 'AIzaSyAd9YIATdY0H83OpK9Xm_ne6hhA_jMrxHA'
     apiKey: 'AIzaSyDJby-hPhgoq4hIhiwKiHYvYmEUn74qnBw'
     // apiKey: 'AIzaSyAnku-8yhTOiYJ9sn1QF0wAU3Qtvlt6GIM'
    }),
    AgmDirectionModule
    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Geolocation,
    NativeGeocoder,
    BackgroundGeolocation,
    LocalNotifications,
    BackgroundMode,
    DatePipe,
    Stripe,
    SMS,
    CallNumber,
    PhonegapLocalNotification
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
