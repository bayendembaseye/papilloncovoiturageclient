import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reserver',
  templateUrl: './reserver.page.html',
  styleUrls: ['./reserver.page.scss'],
})
export class ReserverPage implements OnInit {
reserverForm: FormGroup;
error_messages = {
  client_id: [{ type : 'required', message : 'le nom est obligatoire'},
        {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
      ]}
  constructor(private formbuilder :FormBuilder) { }

  ngOnInit() {
    this.reserverForm = this.formbuilder.group({
      numerocarte: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
    });
  }

}
